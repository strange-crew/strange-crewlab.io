# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Strange Crew'
copyright = '2024, Edward Strange'
author = 'Edward Strange'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
        "ablog",
        "myst_nb",
        "sphinx_copybutton",
        "sphinxext.opengraph"
    ]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '.venv', 'external', "README.md"]

myst_enable_extensions = ["colon_fence"]

myst_links_external_new_tab = True



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'pydata_sphinx_theme'
#import pathlib
#html_theme = "theme"
#html_theme_path = [pathlib.Path(".")]
html_static_path = ['_static']
html_theme_options = {
        "logo": {
            "text": "Strange Crew"
        },
        "navbar_persistent": [],
        "secondary_sidebar_items": ["page-toc"],
        "icon_links": [
            {
                "name": "GitLab",
                "url": "https://gitlab.com/strange-crew",
                "icon": "fa-brands fa-gitlab"
            },
            {
                "name": "LinkedIn",
                "url": "https://www.linkedin.com/in/noah-roberts-63b95535/",
                "icon": "fa-brands fa-linkedin"
            }
        ],
        "article_footer_items": ["mastodon-comments"]
    }

html_sidebars = {
        "*": [],
        "blog/**": [
            "ablog/postcard.html",
            "ablog/recentposts.html",
            "ablog/categories.html",
            "ablog/archives.html",
            "ablog/tagcloud.html"
        ]
    }

html_context = {
        "default_mode": "dark"
    }

# ------------- ABLOG CONFIG ---------------------------------
blog_path = "blog/index"


# -------------- OPENGRAPH ----------------------------------
ogp_site_url = "https://strange-crew.dev"
ogp_image = "/_static/cpp_logo.png"
ogp_image_alt = "C++ Logo"
