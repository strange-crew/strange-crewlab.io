---
html_theme.sidebar_secondary.remove: true
---

# Strange Crew's Strange Website

The layout, theming, and even what tool to use to generate this website is very much a work in progress.  Please bear with us.

Currently we are using Sphinx configured for Markdown and ABlog with the PyData Sphinx Theme.


```{toctree}
:maxdepth: 2

about
contact
blog/index
```

## Recent Blog Posts

```{postlist}
:format: "{title} - {date}"
:date: "%B %d, %Y"
:excerpts:
```
